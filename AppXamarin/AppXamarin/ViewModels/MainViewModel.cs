﻿

namespace AppXamarin.ViewModels
{
    using GalaSoft.MvvmLight.Command;
    using Models;
    using System.Collections.ObjectModel;
    using System.Windows.Input;
    using System;

    public class MainViewModel
    {
        
        #region Properties
        public string Amount { get; set; }
        public ObservableCollection<Rate> Rates { get; set; }
        public Rate SourceRate { get; set; }
        public Rate TargetRate { get; set; }
        public bool IsRunning { get; set; }
        public bool IsEnabled { get; set; }
        public string Result { get; set; }

        #endregion
        public MainViewModel()
        {

        }
        #region Commands
        public ICommand ConverterCommand {
            get
            {
                return new RelayCommand(Converter);
            }
                }

        void Converter()
        {
            throw new NotImplementedException();
        }
        #endregion
    }
}
